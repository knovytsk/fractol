# fractol
This is a small fractal exploration program.

### skills ###
- optimization using library Pthread (Multi-threading implementation)
- mathematical notion of complex numbers

### usage ###

./fractol [fractal name]

YOU CAN CHOOSE ONE OF THESE:

0.mandelbrot

1.julia

2.ships

3.snowman

4.star

5.fish

6.bird

7.man

8.mandelbrot1

9.julia6
